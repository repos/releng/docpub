
.PHONY: build-lint-image build-test-image lint run-tests verify

LINT_IMAGE := docpub-lint-image
TEST_IMAGE := docpub-test-image

build-lint-image:
	DOCKER_BUILDKIT=1 docker build -f .pipeline/blubber.yaml --target lint -t $(LINT_IMAGE) .

build-test-image:
	DOCKER_BUILDKIT=1 docker build -f .pipeline/blubber.yaml --target test -t $(TEST_IMAGE) .

lint: build-lint-image
	docker run -it --rm $(LINT_IMAGE)

run-tests: build-test-image
	docker run -it --rm $(TEST_IMAGE)

verify: lint run-tests
