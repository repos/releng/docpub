#!/usr/bin/python3

import functools
import logging
import os
import sys
import time

import requests

SOURCE_DIR = '/srv/app'
sys.path.insert(0, SOURCE_DIR)

from lib.log import set_up_logging

DOCPUB_JOB_URL = 'https://releases-jenkins.wikimedia.org/job/docpub'

session = requests.Session()
# Note the token is not an actual secret. We use the user/token to work around the problem described in
# https://phabricator.wikimedia.org/T338950
session.auth = ('jenkinsrelapi', '11006dc3b82f367a5a99c3ccd71dafe8c7')
session.request = functools.partial(session.request, allow_redirects=True)


def rest(url, method='get', **kwargs):
    r = getattr(session, method)(url, **kwargs)
    r.raise_for_status()
    return r


def preflight_allowed_project_validation():
    def get_allowed_projects():
        allowed_projects_file = os.path.join(SOURCE_DIR, 'allowed_projects')
        return [line.strip() for line in open(allowed_projects_file) if line.strip()]

    project_path = os.getenv('CI_PROJECT_PATH')
    if project_path not in get_allowed_projects():
        logging.error(
            """Project "%s" is not allowed to publish. Please see README of docpub tool""",
            project_path
        )
        sys.exit(1)


def select_docs_to_publish():
    docs_id = os.getenv('DOCS_ID', 'DEFAULT')
    selected_jwt = os.getenv(docs_id + '_DOC_JWT', '')
    selected_docs_dir = os.getenv(docs_id + '_DOCS_DIR', '')

    for docs_var in selected_jwt, selected_docs_dir:
        if not docs_var.strip():
            if docs_id == 'DEFAULT':
                msg = 'Have you defined a docs build job in your pipeline?'
            else:
                msg = f"""Could not find details for job with identifier "{docs_id}" """
            logging.error(f"Could not determine which build job to use for publishing. {msg}")
            sys.exit(1)

    os.environ['DOC_PRJ_JWT'] = selected_jwt
    os.environ['DOC_PRJ_DOCS_DIR'] = selected_docs_dir


def verify_artifacts():
    artifacts_dir = os.getenv('DOC_PRJ_DOCS_DIR')
    if not os.path.isdir(artifacts_dir) or len(os.listdir(artifacts_dir)) == 0:
        logging.error("Cannot find docs in the specified dir: %s", artifacts_dir)
        sys.exit(1)


def trigger_publish_job():
    job_vars = {
        var: os.getenv(var) for var in [
            'CI_SERVER_HOST',
            'CI_PROJECT_PATH',
            'GITLAB_USER_EMAIL',
            'PUB_LOCATION',
            'COVERAGE',
            'DOC_PRJ_JWT',
            'DOC_PRJ_DOCS_DIR'
        ]
    }
    try:
        buildUrl = f"{DOCPUB_JOB_URL}/buildWithParameters"
        resp = rest(
            buildUrl,
            'post',
            data=job_vars
        )
        location = resp.headers.get('Location')
        if not location:
            raise Exception(f"No Location header in the response when POSTing to {buildUrl}")
        return location
    except Exception as e:
        logging.error("Failed to trigger doc publishing job: %s", e)
        sys.exit(1)


def wait_for_job_to_be_started(job_queue_item):
    def get_job_queue_status():
        try:
            return rest(f"{job_queue_item}api/json").json()
        except Exception as e:
            logging.error("Could not check if doc publishing job started: %s", e)
            sys.exit(1)

    time.sleep(1)
    job_queue_status = get_job_queue_status()

    if job_queue_status['why'] is not None:
        deadline = time.time() + 300

        logging.info("Waiting for up to 5 minutes for the doc publishing job to be started")

        while time.time() < deadline:
            job_queue_status = get_job_queue_status()
            if job_queue_status['why'] is None:
                break
            time.sleep(10)
        else:
            logging.error("Job failed to start after 5 minutes. Aborting")
            sys.exit(1)

    return job_queue_status['executable']['number']


def log_job_location(job_id):
    try:
        job_status = rest(f"{DOCPUB_JOB_URL}/{job_id}/api/json").json()
        logging.info(
            f"Doc publishing job started. You can follow execution and see results at: {job_status['url']}console"
        )
    except Exception as e:
        logging.error("Could not get doc publishing job status: %s", e)
        sys.exit(1)


if __name__ == '__main__':
    set_up_logging()

    preflight_allowed_project_validation()

    select_docs_to_publish()
    verify_artifacts()
    job_queue_item = trigger_publish_job()
    job_id = wait_for_job_to_be_started(job_queue_item)
    log_job_location(job_id)
