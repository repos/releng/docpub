#!/usr/bin/python3

import logging
import os
import re
import subprocess
import sys
import zipfile

import jwt
import requests
from jwt import PyJWKClient, InvalidTokenError

ARTIFACTS_DIR = '/tmp/arts'
SOURCE_DIR = '/srv/app'
sys.path.insert(0, SOURCE_DIR)

from lib.log import set_up_logging


def gl_api_get(path, **kwargs):
    res = requests.get(
        f"https://{os.getenv('CI_SERVER_HOST')}/api/v4/{path}",
        allow_redirects=True,
        **kwargs
    )
    res.raise_for_status()
    return res


# Top-level groups in our GitLab instance follow the convention of adding "repos" to its name. We don't want to pass
# this along to the published location, so we remove that portion. Names then are mapped as follows:
#   * /repos/<project_path> -> <project_path>
#   * /cloudvps-repos/<project_path> -> cloudvps/<project_path>
#   * /toolforge-repos/<project_path> -> toolforge/<project_path>
def normalize_doc_location(path):
    return re.sub(r'^([^/-]*)-?repos', r'\g<1>', path.removeprefix('/')).removeprefix('/')


def validate_non_empty_inputs():
    for user_var in [
        'PUB_LOCATION', 'COVERAGE', 'CI_SERVER_HOST',
        'DOC_PRJ_JWT', 'DOC_PRJ_DOCS_DIR', 'RSYNC_USER'
    ]:
        value = os.getenv(user_var)
        if not value or not value.strip():
            logging.error(
                f"""Value "{user_var}" cannot be empty. Did you accidentally overwrite it in your pipeline"""
                " configuration? Otherwise you've run into a bug"
            )
            sys.exit(1)


def decode_jwt():
    jwks_client = PyJWKClient(f"https://{os.getenv('CI_SERVER_HOST')}/oauth/discovery/keys")

    try:
        token = os.getenv('DOC_PRJ_JWT')
        return jwt.decode(
            token,
            jwks_client.get_signing_key_from_jwt(token).key,
            jwt.get_unverified_header(token)['alg'],
            audience='doc-publisher'
        )
    except InvalidTokenError as e:
        logging.error("Token invalid: %s", e)
        sys.exit(1)


def validate_project_is_allowed(token_data):
    def get_allowed_projects():
        allowed_projects_file = os.path.join(SOURCE_DIR, 'allowed_projects')
        return [line.strip() for line in open(allowed_projects_file) if line.strip()]

    project_path = token_data['project_path'].removesuffix('/')
    if project_path not in get_allowed_projects():
        logging.error("""Project "%s" is not allowed to publish""", project_path)
        sys.exit(1)


def validate_publish_location(token_data):
    normalized_project_path = normalize_doc_location(token_data['project_path'])
    if not normalized_pub_location.startswith(normalized_project_path):
        logging.error(
            """Publishing location "%s" is invalid for project "%s" """, normalized_pub_location, normalized_project_path
        )
        sys.exit(1)


def get_docs(token_data):
    project_id, job_id = (token_data['project_id'], token_data['job_id'])
    logging.info('Retrieving documentation from upstream pipeline')

    try:
        job_res = gl_api_get(f"projects/{project_id}/jobs/{job_id}/artifacts", stream=True)

        artifacts_zip = '/tmp/artifacts.zip'
        with open(artifacts_zip, 'wb') as artsf:
            # 10 MB chunks (base 10)
            for chunk in job_res.iter_content(chunk_size=10000000):
                artsf.write(chunk)
        job_res.close()
        with zipfile.ZipFile(artifacts_zip, 'r') as artsf:
            artsf.extractall(ARTIFACTS_DIR)
    except Exception as e:
        logging.error("Failed to retrieve documentation from upstream pipeline: %s", e)
        sys.exit(1)

    if os.path.isdir(docs_dir) and len(os.listdir(docs_dir)) > 0:
        logging.info('Documentation retrieved')
    else:
        logging.error(f"""Specified docs directory "{docs_dir}" not found among the upstream artifacts or is empty""")
        sys.exit(1)


def publish_docs():
    def get_final_doc_location():
        if os.getenv('COVERAGE').lower() in ['true', 'yes', 'y']:
            return 'cover/' + normalized_pub_location
        return normalized_pub_location

    def create_docs_dir_at_destination():
        """
        Rsync empty dir to ensure the publishing location exists on the target
        """
        try:
            os.makedirs(f"/tmp/{final_doc_location}")
            # Rsync password must be provided in environment variable RSYNC_PASSWORD
            subprocess.check_call([
                'rsync', '--recursive', '--relative',
                final_doc_location, f"rsync://{rsync_user}@doc.discovery.wmnet/doc-auth/"
            ], cwd='/tmp')
        except Exception as e:
            logging.error("Failed to create directory for documentation at target destination: %s", e)
            sys.exit(1)

    def rsync_docs():
        try:
            # Note that `--archive` will preserve permissions and dates, but owner and group are assigned by the
            # rsync module on the target (i.e. `doc-uploader`).
            # Rsync password must be provided in environment variable RSYNC_PASSWORD
            subprocess.check_call([
                'rsync', '--archive', '--compress', '--delete-after',
                docs_dir + '/', f"rsync://{rsync_user}@doc.discovery.wmnet/doc-auth/{final_doc_location}/"
            ])
        except subprocess.CalledProcessError as e:
            logging.error("Failed to publish documentation: %s", e)
            sys.exit(1)

    logging.info('Publishing documentation')

    final_doc_location = get_final_doc_location()
    create_docs_dir_at_destination()
    rsync_docs()

    logging.info(f"Done!. You can find the documentation at https://doc.wikimedia.org/{final_doc_location}")


if __name__ == '__main__':
    set_up_logging(True)

    validate_non_empty_inputs()

    rsync_user = os.getenv('RSYNC_USER')
    docs_dir = os.path.join(ARTIFACTS_DIR, os.getenv('DOC_PRJ_DOCS_DIR'))
    normalized_pub_location = normalize_doc_location(os.getenv('PUB_LOCATION'))

    token_data = decode_jwt()
    validate_project_is_allowed(token_data)
    validate_publish_location(token_data)

    get_docs(token_data)
    publish_docs()
