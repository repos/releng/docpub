import os
import shutil
from unittest import mock

import pytest

from bin import docpub
from tests.conftest import FIXTURES_PATH


@pytest.fixture(autouse=True)
def inject_values():
    docpub.SOURCE_DIR = FIXTURES_PATH
    docpub.normalized_pub_location = docpub.normalize_doc_location(os.getenv('PUB_LOCATION'))
    docpub.docs_dir = os.path.join(docpub.ARTIFACTS_DIR, os.getenv('DOC_PRJ_DOCS_DIR'))
    docpub.rsync_user = os.getenv('RSYNC_USER')


@pytest.fixture(autouse=True)
def response(mocker):
    return mocker.patch('requests.Response', autospec=True)


@pytest.fixture(autouse=True)
def gl_api_get(mocker, response):
    gl_api_get = mocker.patch('bin.docpub.gl_api_get')
    gl_api_get.return_value = response
    return gl_api_get


input_names = [
    'PUB_LOCATION', 'COVERAGE', 'CI_SERVER_HOST',
    'DOC_PRJ_JWT', 'DOC_PRJ_DOCS_DIR', 'RSYNC_USER'
]


@pytest.fixture(params=input_names)
def missing_inputs(request):
    # Clear one input var at a time
    os.environ[request.param] = ''


def test_successful_validate_non_empty_inputs():
    docpub.validate_non_empty_inputs()


@mock.patch('logging.error')
def test_failed_validate_non_empty_inputs(logging, missing_inputs, request):
    with pytest.raises(SystemExit):
        docpub.validate_non_empty_inputs()

        error_msg = logging.logging.call_args.args[0]
        assert request.param in error_msg


@mock.patch('jwt.PyJWKClient.__init__', return_value=None)
@mock.patch('jwt.get_unverified_header', return_value={'alg': 'le alg'})
@mock.patch('jwt.api_jwk.PyJWK', autospec=True)
@mock.patch('jwt.decode', return_value='le decoded')
def test_decode_jwt(jwt_decode, mock_key, jwt_get_unverified_header, jwkclient_init, mocker):
    mock_key.key = 'le key'
    jwkclient_get_signing_key_from_jwt = mocker.patch('jwt.PyJWKClient.get_signing_key_from_jwt', return_value=mock_key)

    decoded_token = docpub.decode_jwt()

    jwkclient_init.assert_called_with('https://le_host/oauth/discovery/keys')
    jwkclient_get_signing_key_from_jwt.assert_called_with('le jwt')
    jwt_get_unverified_header.assert_called_with('le jwt')
    jwt_decode.assert_called_with(
        'le jwt',
        'le key',
        'le alg',
        audience='doc-publisher'
    )
    assert decoded_token == 'le decoded'


def test_validate_project_is_allowed():
    docpub.validate_project_is_allowed({'project_path': 'allowed/project'})
    docpub.validate_project_is_allowed({'project_path': 'also/allowed'})
    with pytest.raises(SystemExit):
        docpub.validate_project_is_allowed({'project_path': 'not/you'})


def test_validate_publish_location():
    docpub.validate_publish_location({'project_path': 'repos/le/path'})
    with pytest.raises(SystemExit):
        docpub.validate_publish_location({'project_path': 'repos/different/path'})


def test_successful_get_docs(gl_api_get, response):
    expected_docs_dir = os.path.join(docpub.ARTIFACTS_DIR, 'le/dir')
    if os.path.exists(expected_docs_dir):
        shutil.rmtree(expected_docs_dir)
    response.iter_content.return_value = open(os.path.join(FIXTURES_PATH, 'art_right_docs_path.zip'), 'rb').readlines()

    docpub.get_docs({
        'project_id': 123,
        'job_id': 321,
    })

    gl_api_get.assert_called_with('projects/123/jobs/321/artifacts', stream=True)
    assert os.path.isdir(expected_docs_dir)


def test_failed_get_docs(gl_api_get, response):
    """
    In this case the downloaded artifact does not contain the docs at the location specified by DOC_PRJ_DOCS_DIR
    """
    expected_docs_dir = os.path.join(docpub.ARTIFACTS_DIR, 'le/dir')
    if os.path.exists(expected_docs_dir):
        shutil.rmtree(expected_docs_dir)
    response.iter_content.return_value = open(os.path.join(FIXTURES_PATH, 'art_wrong_docs_path.zip'), 'rb').readlines()

    with pytest.raises(SystemExit):
        docpub.get_docs({
            'project_id': 123,
            'job_id': 321,
        })

        gl_api_get.assert_called_with('projects/123/jobs/321/artifacts', stream=True)
        assert not os.path.isdir(expected_docs_dir)


@mock.patch('subprocess.check_call')
def test_publish_docs(subpr):
    expected_final_doc_location = 'le/path/loc'
    if os.path.exists(f"/tmp/{expected_final_doc_location}"):
        shutil.rmtree(f"/tmp/{expected_final_doc_location}")
    expected_rsync_user = 'le rsyncer'

    docpub.publish_docs()

    subpr.assert_has_calls([
        mock.call([
            'rsync', '--recursive', '--relative',
            expected_final_doc_location,
            f"rsync://{expected_rsync_user}@doc.discovery.wmnet/doc-auth/"
        ], cwd='/tmp'),
        mock.call([
            'rsync', '--archive', '--compress', '--delete-after',
            os.path.join(docpub.ARTIFACTS_DIR, 'le/dir') + '/',
            f"rsync://{expected_rsync_user}@doc.discovery.wmnet/doc-auth/{expected_final_doc_location}/"
        ])
    ])


@mock.patch('subprocess.check_call')
def test_cover_publish_docs(subpr):
    expected_final_doc_location = 'cover/le/path/loc'
    if os.path.exists(f"/tmp/{expected_final_doc_location}"):
        shutil.rmtree(f"/tmp/{expected_final_doc_location}")
    expected_rsync_user = 'le rsyncer'
    os.environ['COVERAGE'] = 'yes'

    docpub.publish_docs()

    subpr.assert_has_calls([
        mock.call([
            'rsync', '--recursive', '--relative',
            expected_final_doc_location,
            f"rsync://{expected_rsync_user}@doc.discovery.wmnet/doc-auth/"
        ], cwd='/tmp'),
        mock.call([
            'rsync', '--archive', '--compress', '--delete-after',
            os.path.join(docpub.ARTIFACTS_DIR, 'le/dir') + '/',
            f"rsync://{expected_rsync_user}@doc.discovery.wmnet/doc-auth/{expected_final_doc_location}/"
        ])
    ])
