import os
import sys

import pytest

sys.path.insert(0, os.path.normpath(os.path.join(__file__, "../..")))

FIXTURES_PATH = os.path.join(os.path.dirname(__file__), 'fixtures')


def pytest_configure(config):
    class GlobalFixtures:
        @pytest.fixture(autouse=True)
        def env(self):
            os.environ = {
                'CI_SERVER_HOST': 'le_host',
                'CI_PROJECT_PATH': 'repos/le/path',
                'GITLAB_USER_EMAIL': 'le@mail.com',
                'PUB_LOCATION': 'repos/le/path/loc',
                'COVERAGE': 'nope',
                'DOC_PRJ_JWT': 'le jwt',
                'DOC_PRJ_DOCS_DIR': 'le/dir',
                'RSYNC_USER': 'le rsyncer',
            }

        @pytest.fixture(autouse=True)
        def sleep(self, mocker):
            sleep = mocker.patch('time.sleep')
            sleep.side_effect = lambda _: None
            return sleep

    config.pluginmanager.register(GlobalFixtures())
