import os
import time
from unittest import mock

import pytest

from bin import trigger_docpub
from tests.conftest import FIXTURES_PATH

DOCPUB_JOB_URL = 'http://no.where'


@pytest.fixture(autouse=True)
def mock_docpub_job_url(mocker):
    return mocker.patch.object(trigger_docpub, 'DOCPUB_JOB_URL', DOCPUB_JOB_URL)


@pytest.fixture(autouse=True)
def response(mocker):
    return mocker.patch('requests.Response', autospec=True)


@pytest.fixture(autouse=True)
def rest(mocker, response):
    rest = mocker.patch('bin.trigger_docpub.rest')
    rest.return_value = response
    return rest


def test_successful_preflight_allowed_project_validation():
    os.environ['CI_PROJECT_PATH'] = 'allowed/project'
    trigger_docpub.SOURCE_DIR = FIXTURES_PATH

    trigger_docpub.preflight_allowed_project_validation()


@mock.patch('logging.error')
def test_failed_preflight_allowed_project_validation(logging):
    trigger_docpub.SOURCE_DIR = FIXTURES_PATH
    with pytest.raises(SystemExit):
        trigger_docpub.preflight_allowed_project_validation()

        msg = logging.call_args.args[0]
        assert 'is not allowed to publish' in msg


def test_successful_select_docs_to_publish():
    os.environ['DOCS_ID'] = 'SOME_JOB'
    os.environ['SOME_JOB_DOC_JWT'] = 'token'
    os.environ['SOME_JOB_DOCS_DIR'] = 'dir'

    trigger_docpub.select_docs_to_publish()

    assert os.getenv('DOC_PRJ_JWT') == 'token'
    assert os.getenv('DOC_PRJ_DOCS_DIR') == 'dir'


job_ids = ['DEFAULT', 'SOME_JOB']


@pytest.mark.parametrize('job_id', job_ids)
@mock.patch('logging.error')
def test_failed_select_docs_to_publish(logging, job_id):
    os.environ['DOCS_ID'] = job_id
    with pytest.raises(SystemExit):
        trigger_docpub.select_docs_to_publish()

        msg = logging.call_args.args[0]
        assert 'Could not determine which build job to use for publishing' in msg
        if job_id == 'DEFAULT':
            assert 'DEFAULT' not in msg
        else:
            assert job_id in msg


@mock.patch('logging.error')
def test_verify_artifacts(logging):
    with pytest.raises(SystemExit):
        trigger_docpub.verify_artifacts()

        msg = logging.call_args.args[0]
        assert 'le/dir' in msg


def test_trigger_publish_job(rest, response):
    response.headers = {'Location': 'job_queue_item_location'}

    job_queue_item = trigger_docpub.trigger_publish_job()

    rest.assert_called_with(
        f"{DOCPUB_JOB_URL}/buildWithParameters",
        'post',
        data={
            'CI_SERVER_HOST': 'le_host',
            'CI_PROJECT_PATH': 'repos/le/path',
            'GITLAB_USER_EMAIL': 'le@mail.com',
            'PUB_LOCATION': 'repos/le/path/loc',
            'COVERAGE': 'nope',
            'DOC_PRJ_JWT': 'le jwt',
            'DOC_PRJ_DOCS_DIR': 'le/dir'
        }
    )
    assert job_queue_item == 'job_queue_item_location'


def test_successful_wait_for_job(rest, response):
    response.json.side_effect = [
        {
            'why': 'job still in the queue'
        }, {
            #  Job completed
            'why': None,
            'executable': {
                'number': 123
            }
        }
    ]

    job_queue_item = 'http://some.place/'
    job_id = trigger_docpub.wait_for_job_to_be_started(job_queue_item)

    rest.assert_called_with(f"{job_queue_item}api/json")
    assert job_id == 123


def test_timeout_wait_for_job(mocker, rest, response):
    first_time_call = True
    real_time = time.time

    def mocked_time():
        nonlocal first_time_call
        if first_time_call:
            first_time_call = False
            # When the code is creating its deadline, make it believe 5 minutes are almost up
            return real_time() - 299
        return real_time()

    time_mock = mocker.patch('time.time')
    time_mock.side_effect = mocked_time
    response.json.return_value = {'why': 'job still in the queue'}

    with pytest.raises(SystemExit):
        job_queue_item = 'http://some.place/'
        trigger_docpub.wait_for_job_to_be_started(job_queue_item)

        rest.assert_called_with(f"{job_queue_item}api/json")


@mock.patch('logging.info')
def test_log_job_location(logging, rest, response):
    response.json.return_value = {'url': 'http://job.location/'}

    trigger_docpub.log_job_location(123)

    rest.assert_called_with(f"{DOCPUB_JOB_URL}/{123}/api/json")
    msg = logging.call_args.args[0]
    assert 'http://job.location/console' in msg
