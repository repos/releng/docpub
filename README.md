# docpub

This project provides two GitLab CI templates contained in `publish.yml` which can be used to publish documentation to
`doc.wikimedia.org`.

From your `.gitlab-ci.yml`, import `publish.yml` using a [GitLab include](https://docs.gitlab.com/ee/ci/yaml/includes.html).
Your project will also need to be added to the list of [allowed projects](https://gitlab.wikimedia.org/repos/releng/docpub/-/blob/main/allowed_projects).

## Templates
### `.docpub:build-docs`

Extend this template in one of your jobs and build your docs in it.

Note the template uses an `after_script` block, which means your extending job cannot use an `after_script` of its own.
That would overwrite the template's behavior and cause a failure.

Variables:
 * `DOCS_DIR`: Directory where your job is generating the docs. Default is `docs`
 * `DOCS_ID`: Identifier for your docs that can be referenced later by a publishing job. Default is `DEFAULT`

### `.docpub:publish-docs`

Extend this template in one of your jobs. It will trigger a publishing job in the
[releases Jenkins](https://releases-jenkins.wikimedia.org/job/docpub/).

If your job uses the `needs` keyword, you will need to include your doc building job in the list of needed jobs.
Otherwise, your publishing job will not have access to the artifacts from previous stages as mentioned in
[the docs](https://docs.gitlab.com/ee/ci/yaml/#needsartifacts). This will make publishing fail.

Variables:
* `PUB_LOCATION`: Path on `doc.wikimedia.org` where you want the docs to be published. Must begin with your project's
path.

  The final publishing URL modifies GitLab's top-level groups using the following mapping:
    * /repos/<project_path> -> <project_path>
    * /cloudvps-repos/<project_path> -> cloudvps/<project_path>
    * /toolforge-repos/<project_path> -> toolforge/<project_path>

  Note that the job will display in the console the full URL where your docs are being published.

  See also variable `COVERAGE`.

  Default is your project's path


* `COVERAGE`: Whether the docs are a test coverage report. PUB_LOCATION will be pre-appended with `cover`.

  Default is False.

* `DOCS_ID`: Identifier for the docs to be used. Must reference a previous building job with the same identifier.

  Default is `DEFAULT`

### Example usage

Use two different stages to build the docs first and publish second. Make sure you're generating the docs in the
directory indicated by `DOCS_DIR`. You also probably want to publish only from your default branch or some other
specially designated ref. For instance:

```yaml
include:
  - project: repos/releng/docpub
    file: includes/publish.yml

stages:
  - build-docs-stage
  - publish-docs-stage
    
.docs-job:
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

build-job:
  stage: build-docs-stage
  extends:
    - .docs-job
    - .docpub:build-docs
  variables:
    DOCS_DIR: generated-documentation/docs
  script:
    - ./generate-documentation.sh

publish-job:
  stage: publish-docs-stage
  extends:
    - .docs-job
    - .docpub:publish-docs
  variables:
    PUB_LOCATION: 'your/desired/location'
```

If you have multiple jobs generating docs, you can also explicitly pass the `DOCS_ID` variable to select which docs to
publish. For example, if you're publishing the coverage reports for tests running in parallel:

```yaml
[...]

tests-job-one:
  stage: tests
  extends:
    - .docpub:build-docs
  variables:
    DOCS_DIR: coverage
    DOCS_ID: docs_one
  script:
    ...

tests-job-two:
  stage: tests
  extends:
    - .docpub:build-docs
  variables:
    DOCS_DIR: coverage
    DOCS_ID: docs_two
  script:
    ...

[...]

publish-job-one:
  stage: publish-docs-stage
  extends:
    - .docpub:publish-docs
  variables:
    PUB_LOCATION: 'location/report/one'
    COVERAGE: true
    DOCS_ID: docs_one

publish-job-two:
  stage: publish-docs-stage
  extends:
    - .docpub:publish-docs
  variables:
    PUB_LOCATION: 'location/report/two'
    COVERAGE: true
    DOCS_ID: docs_two

```